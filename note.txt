NASTAVENÍ ALIASU PRO PHP A COMPOSER NA SSH
$ which php
/usr/bin/php

$ /usr/bin/php -v
PHP 7.1.33

$ php -v
PHP 7.2.24

$ type -a php
php is aliased to '/usr/bin/php7.4'
php is /usr/bin/php

$ which composer
/usr/local/bin/composer

alias php="/usr/bin/php7.4"
alias composer="/usr/bin/php7.4 /usr/local/bin/composer"

NASTAVENÍ PRÁV PRO STORAGE A SIMLINK
php artisan storage:link

chmod -R gu+w storage

chmod -R guo+w storage

php artisan cache:clear
