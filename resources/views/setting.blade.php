@extends('layouts.page')

@section('title', '| Nastavení')

@section('content')

    <div class="container">
        <div class="text-center mb-4 mt-4">
            <h3 class="font-weight-bolder text-uppercase">Nastavení</h3>
        </div>
        <div id="app" class="card mb-4 border-0 col-md-6 mr-auto ml-auto">
            <div class="card-body">
{{--                @TODO dodělat vue--}}
                {!! Form::open(['method'=>'POST', 'class'=>'row', 'autocomplete' => 'off', 'action' => 'App\Http\Controllers\Pages\SettingPageController@updateSetting']) !!}
                    {!! Honeypot::generate('my_name', 'my_time') !!}
                    <div class="form-group form-group--custom">
                        <label for="name">Jméno <span class="text-danger">*</span></label>
                        <input name="name" type="text" class="form-control" id="name" value="{{$user->name}}" required>
                    </div>
                    <div class="form-group form-group--custom">
{{--                        :class="cinError == null ? 'mb-0' : 'mb-3'"--}}
                        <label for="cin">IČO <span class="text-danger">*</span></label>
{{--                        <div class="w-100">--}}
                            <input name="cin" type="text" class="form-control" id="cin" value="{{$user->ico}}" required>
{{--                            <small v-if="cinError" id="cin" class="form-text text-muted">@{{ cinError }}</small>--}}
{{--                        </div>--}}
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="tin">DIČ</label>
                        <input name="tin" type="text" class="form-control" id="tin" value="{{$user->dic}}">
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="street">Ulice</label>
                        <input name="street" type="text" class="form-control" id="street" value="{{$user->street}}">
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="city">Město</label>
                        <input name="city" type="text" class="form-control" id="city" value="{{$user->city}}">
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="zip">PSČ</label>
                        <input name="zip" type="text" class="form-control" id="zip" value="{{$user->postcode}}">
                    </div>

                    <div class="text-md-right">
                        <button type="submit" class="btn btn-primary">Uložit</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

@include('includes.script')
