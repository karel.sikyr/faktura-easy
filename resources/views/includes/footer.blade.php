<footer class="container-fluid p-0 bg-white mt-5">
    <div class="card border-0">
        <div class="card-body shadow-none">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md">
                        <div class="d-flex">
                            <img src="{{asset('svg/bill.svg')}}" class="w-25 img-fluid mb-2" alt="easy-faktura.cz">
                            <div class="ml-4">
                                <small class="d-block text-muted">www.easy-faktura.cz</small>
                                <small class="d-block text-muted">© {{ now()->year }}</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md">
                        <h5>Důležité dokumenty</h5>
                        <ul class="list-unstyled text-small">
                            <li><a class="text-muted" href="#">Smluvní podmínky</a></li>
                            <li><a class="text-muted" href="#">Ochrana osobních údajů</a></li>
                            <li><a class="text-muted" href="#">Cookie Policy</a></li>
                        </ul>
                    </div>
                    <div class="col-6 col-md">
                        <h5>Návody</h5>
                        <ul class="list-unstyled text-small">
                            <li><a class="text-muted" href="#">Manuál</a></li>
                            <li><a class="text-muted" href="#">Vzory faktur</a></li>
                            <li><a class="text-muted" href="#">Často kladené otázky</a></li>
                            <li><a class="text-muted" href="#">Kontakt</a></li>
                        </ul>
                    </div>
                    <div class="col-6 col-md">
                        <h5>Další informace</h5>
                        <ul class="list-unstyled text-small">
                            <li><a class="text-muted" href="{{route('contact')}}">Kontaktní formulář</a></li>
                            <li><a class="text-muted" href="{{route('contact')}}">Nápad na zlepšení</a></li>
                            <li><a class="text-muted" href="{{route('contact')}}">Report chyb</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
