<div class="modal fade" id="register" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Registrace</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('register') }}">
                    @csrf

{{--                    {!! Honeypot::generate('my_name', 'my_time') !!}--}}

                    <div class="form-group">
                        <label for="registrationName">* Jméno</label>
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="registrationName" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="registrationEmail">* E-mail</label>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="registrationEmail" aria-describedby="emailHelp" value="{{ old('email') }}" required autocomplete="email">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="registrationPassword">* Heslo</label>
                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="registrationPassword" required autocomplete="new-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="registrationPasswordForCheck">* Heslo pro kontrolu</label>
                        <input type="password" name="password_confirmation" class="form-control" id="registrationPasswordForCheck" required autocomplete="new-password">
                    </div>
                    <button type="submit" class="btn btn-primary">Zaregistrovat se</button>
                </form>
            </div>
        </div>
    </div>
</div>
