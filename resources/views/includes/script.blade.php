@section('script')
    <script>
        var subscribers = @json($subscribers);
        var invoiceData = @json($invoiceData);
        var getDataOfPersonByApi = @json(route('get.data-of-person-by-api'));
        var getInvoiceDate = @json(route('get.invoice-data'));
        var getSubscriberData = @json(route('get.subscriber-data'));
    </script>
@endsection
