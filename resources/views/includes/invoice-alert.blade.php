@if(session()->has('invoice-alert'))
    <div class="alert alert-{{session()->get('invoice-alert')['type']}} alert-dismissible fade show mb-0" role="alert">
        <strong>{{session()->get('invoice-alert')['message']}}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
