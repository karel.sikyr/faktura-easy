<div class="modal fade" id="login" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('login') }}">
                    @csrf

{{--                    {!! Honeypot::generate('my_name', 'my_time') !!}--}}

                    <div class="form-group">
                        <label for="loginEmail">* E-mail</label>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror"
                               id="loginEmail" aria-describedby="emailHelp" value="{{ old('email') }}" required
                               autocomplete="email" autofocus>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="loginPassword">* Heslo</label>
                        <input type="password" name="password"
                               class="form-control @error('password') is-invalid @enderror" id="loginPassword" required
                               autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <a href="{{route('lost-password')}}">Zapomenutů heslo</a>
                    </div>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="loginStayLogOn"
                               name="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="form-check-label" for="loginStayLogOn">Zůstat přilášen</label>
                    </div>
                    <button type="submit" class="btn btn-primary">Přihlásit se</button>
                </form>
            </div>
        </div>
    </div>
</div>
