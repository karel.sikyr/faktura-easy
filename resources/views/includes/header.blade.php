<header class="container-fluid pl-0 pr-0 header fixed-top bg-white shadow-sm">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <h2 class="my-0 mr-md-auto font-weight-bolder text-uppercase"><a href="{{route('welcome')}}" class="text-dark text-decoration-none"><span class="text-primary">Easy</span>-Faktura</a></h2>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse mt-4 mt-md-0" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item mb-2 mb-md-0">
                        <a class="nav-link text-white btn btn-success header__link mr-md-1 d-flex d-md-inline-block justify-content-center" href="{{route('invoiceCreate')}}">
                            <img src="{{asset('svg/plus.svg')}}" class="img-fluid mr-1" alt="">
                            Vystavit fakturu
                        </a>
                    </li>
                    <li class="nav-item mb-2 mb-md-0">
                        <a class="nav-link text-white btn btn-primary header__link d-flex d-md-inline-block justify-content-center" href="{{route('invoiceList')}}">
                            <img src="{{asset('svg/list.svg')}}" class="img-fluid mr-1" alt="">
                            Faktury
                        </a>
                    </li>
                    @auth
                        <li class="nav-item nav-item--user mb-2 mb-md-0 dropdown">
                            <a class="nav-link text-white btn btn-secondary ml-md-4 dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{asset('svg/user-white.svg')}}" class=" img-fluid" alt="Faktura">
                                {{Auth::user()->name}}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{route('setting')}}">Nastavení účtu</a>
                                <a class="dropdown-item" href="{{route('subscribers')}}">Odběratelé</a>
                                <a class="dropdown-item" href="#"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form1').submit();">
                                    Odhlásit se
                                </a>
                                <form id="logout-form1" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    @else
{{--                        <li class="nav-item mb-2 mb-md-0">--}}
{{--                            <a class="nav-link text-white btn btn-primary ml-md-4 mr-md-1" href="#" data-toggle="modal" data-target="#register">Registrace</a>--}}
{{--                        </li>--}}
                        <li class="nav-item mb-2 mb-md-0">
                            <a class="nav-link text-white btn btn-secondary ml-md-4 mr-md-1" href="#" data-toggle="modal" data-target="#login">Login</a>
                        </li>
                    @endauth
                </ul>
            </div>
        </nav>
    </div>
</header>
