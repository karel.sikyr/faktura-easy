@extends('layouts.page')

@section('title', '| Kontakt')

@section('content')

    <div class="container">
        <div class="text-center mb-4 mt-4">
            <h3 class="font-weight-bolder text-uppercase">Kontakt</h3>
        </div>
        <div class="card mb-4 border-0 mr-auto ml-auto">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <h5>Vaše zpětná vazba je pro nás důložitá!</h5>
                        <br>
                        <p>
                            V případě že nám chcete cokoliv zdělit, použijte prosím kontaktní formulář. <br>
                            Oceníme také jakýkoliv nápad na zlepšení, či report případné chyby.
                        </p>

                        <p>
                            O schválení vaší PDF šablony nám psát nemusíte. Všechny požadavky vyřidíme v nejbližší době.
                        </p>

                        <p>
                            V případě že by jste měli zájem naš projekt podpořit. Pokračujte prosím zde <a href="#">Donate</a>
                        </p>

                    </div>
                    <div class="col-md-6">
                        <form method="POST" action="">
                            @csrf
                            {!! Honeypot::generate('my_name', 'my_time') !!}
                            <div class="form-group">
                                <label for="loginEmail">* E-mail</label>
                                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror"
                                       id="loginEmail" aria-describedby="emailHelp" value="{{ old('email') }}" required
                                       autocomplete="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="subject">Předmět</label>
                                <input type="text" name="subject" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="subject">* Zpráva</label>
                                <textarea type="text" name="text" class="form-control" cols="" rows="4" required></textarea>
                                @error('text')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary">Odeslat</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
