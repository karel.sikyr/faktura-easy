<html lang="cs">
<head>
    <title>Easy-faktura</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <style type="text/css">

        body {
            font-family: DejaVu Sans, sans-serif;
            position: relative;
        }

        table {
            /*border-collapse: collapse;*/
            font-size: x-small;
            border-spacing: 0;
        }

        thead th {
            font-weight: normal;
        }

        .invoice-header-author p, .invoice-header-delivery p {
            margin: 0;
        }

        .invoice-header-number h3, .invoice-header-number h4 {
            margin: 0;
        }

        .vertical-align-bottom {
            vertical-align: bottom;
        }

        .vertical-align-top {
            vertical-align: top;
        }

        hr {
            margin-top: 20px;
            margin-bottom: 1px;
            opacity: 0.2;
        }

    </style>

</head>
<body>

<table width="100%">
    <tr class="vertical-align-bottom">
        <td valign="left" class="invoice-header-author">
            <p>{{$invoice->name}}</p>
            <p>{{$invoice->street}}</p>
            <p>{{$invoice->postcode}}, {{$invoice->city}}</p>
{{--            <p>{{$invoice->country->name}}</p>--}}
            <br>
            <p>IČO: {{$invoice->ico}}</p>
            <p>Neplátce DPH </p>
        </td>
        <td align="right" class="invoice-header-number">
            <h3>Faktura č. {{$invoice->reference_number}}</h3>
            <h4>Evidenční číslo: {{$invoice->evidence_number}}</h4>
        </td>
    </tr>
</table>

<hr>

<table width="100%">
    <tr class="vertical-align-top">
        <td valign="left" class="invoice-header-delivery" style="width: 30%">
            <h3>Odběratel</h3>
            <p>{{$invoice->invoiceSubscriber->name}}</p>
            <p>{{$invoice->invoiceSubscriber->street}}</p>
        </td>
        <td align="right" style="text-align: center; border: 1px solid">
            <h5>VYSTAVENO</h5>
            <p>{{$invoice->date_of_issue->format('d.m.Y')}}</p>
        </td>
        <td align="right" style="background: #7a7f86; text-align: center; color: white;">
            <h5>CELKEM</h5>
            <p>@money($invoice->invoiceLists->sum('total_price')) Kč</p>
        </td>
        <td align="right" style="text-align: center; border: 1px solid">
            <h5>SPLATNOST</h5>
            <p>{{$invoice->due_date->format('d.m.Y')}}</p>
        </td>
    </tr>
    <tr class="vertical-align-top">
        <td valign="left" class="invoice-header-delivery">
            <p>{{$invoice->invoiceSubscriber->postcode}}, {{$invoice->invoiceSubscriber->city}}</p>
            <p>{{$invoice->invoiceSubscriber->country->name}}</p>
            <br>
            <p>IČO: {{$invoice->invoiceSubscriber->ico}}</p>
            <p>DIČ: {{$invoice->invoiceSubscriber->dic}}</p>
        </td>
        <td></td>
        <td align="right" style="font-size: x-small" class="invoice-header-delivery">
            <br>
            <p>Forma úhrady:</p>
            <p>Variabilní symbol:</p>
            <p>Bankovní účet:</p>
            <p>IBAN/SWITF:</p>
        </td>
        <td align="right" style="font-size: x-small" class="invoice-header-delivery">
            <br>
            <p>{{$invoice->invoice_payment_type}}</p>
            <p>{{$invoice->variable_symbol}}</p>
            <p>{{$invoice->bank_account_number}}</p>
            <p>{{$invoice->iban_account_number}}</p>
        </td>
    </tr>
</table>

<br>
<br>

<table width="100%" style="border-spacing: 10px;">
    <thead style="border-bottom: 2px solid grey;">
    <tr>
        <th style="text-align: center; width: 8%">Počet</th>
        <th style="text-align: left; width: 62%">Popis</th>
        <th style="width: 15%">Jedn. cena </th>
        <th style="width: 15%">Celkem</th>
    </tr>
    </thead>
    <tbody>
    @foreach($invoice->invoiceLists()->get() as $item)
        <tr>
            <td style="text-align: center">{{$item->counter}}</td>
            <td>{{$item->note}}</td>
            <td align="right" style="text-align: center">@money($item->price) Kč</td>
            <td align="right" style="text-align: center">@money($item->total_price) Kč</td>
        </tr>
    @endforeach
    </tbody>

    <tfoot>
    {{--když je dph--}}
    {{--    <tr>--}}
    {{--        <td colspan=""></td>--}}
    {{--        <td align="right">Mezisoučet</td>--}}
    {{--        <td align="right">2 000,00 Kč</td>--}}
    {{--    </tr>--}}
    {{--    <tr>--}}
    {{--        <td colspan="2"></td>--}}
    {{--        <td align="right">DPH 21%</td>--}}
    {{--        <td align="right">2 420,00 Kč</td>--}}
    {{----}}
    {{--    </tr>--}}
    {{--když je dph--}}
    <tr>
        <td colspan="4" align="right"><h4>Celkem k úhradě: <span style="font-size: 20px"> @money($invoice->invoiceLists->sum('total_price')) Kč</span></h4></td>
    </tr>
    <tr>
        <td colspan="4" align="right">
{{--            QR CODE HERE--}}
        </td>
    </tr>
    </tfoot>
</table>


<div style="position: absolute; left: 0; right: 0; bottom: 0; text-align: center; font-size: x-small">
    <p>Fyzická osoba zapsaná v živnostenském rejstříku.</p>
    <p>Fakturu vystavil: www.easy-faktura.cz</p>
</div>

</body>
</html>
