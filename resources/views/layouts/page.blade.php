<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Easy Faktura @yield('title')</title>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    @yield('css')

</head>
<body>

@include('includes.header')

@include('includes.login')
@include('includes.register')

<div class="content">
    @yield('content')
</div>

@include('includes.footer')

<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/bs.bundle.min.js')}}"></script>

@yield('script')

<script>
    setTimeout(function(){
        $(".alert").alert('close')
    }, 1000);

    @if(session()->has('empty-auth'))
        $('#login').modal('toggle')
    @endif
</script>

<script src="{{asset('js/app.js')}}"></script>

</body>
</html>
