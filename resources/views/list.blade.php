@extends('layouts.page')

@section('title', '| Přehled faktur')

@section('content')

    <div class="container">
        <div class="text-center mb-4 mt-4">
            <h3 class="font-weight-bolder text-uppercase mb-0">Vystavené faktury</h3>
        </div>

        @include('includes.invoice-alert')

        <div class="card mb-4 border-0 shadow-sm">
            <div class="card-body">
                <div class="d-flex justify-content-between">
                    <div class="d-flex">
                        <div class="dropdown">
                            <button class="btn btn-outline-dark dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Hromadné akce
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Stáhnout faktury</a>
                                <a class="dropdown-item" href="#">Smazat faktury</a>
                                <a class="dropdown-item" href="#">Odeslat faktury</a>
                            </div>
                        </div>
                    </div>
                    <form>
                        <input class="form-control mr-sm-2" type="text" placeholder="Hledat" aria-label="Search">
                    </form>
                </div>
            </div>
                <table class="table table-striped mb-0">
                    <thead class="thead-dark">
                    <tr class="mb-4">
                        <th scope="col" class="text-center bg-primary border-0">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="invoice-id">
                                <label class="custom-control-label pointer" for="invoice-id"></label>
                            </div>
                        </th>
                        <th scope="col" class="bg-primary border-0">Číslo</th>
                        <th scope="col" class="bg-primary border-0">Oběratel</th>
                        <th scope="col" class="bg-primary border-0">Splatnost</th>
                        <th scope="col" class="bg-primary border-0">Typ faktury</th>
                        <th scope="col" class="bg-primary border-0">Částka</th>
                        <th scope="col" class="bg-primary border-0"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($invoices as $key => $invoice)
                    <tr>
                        <td class="text-center">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input invoice-checkbox" id="invoice-id-{{$key}}">
                                <label class="custom-control-label pointer" for="invoice-id-{{$key}}"></label>
                            </div>
                        </td>
                        <td>{{$invoice->reference_number}}</td>
                        <td>{{$invoice->invoiceSubscriber->name}}</td>
                        <td>{{$invoice->due_date->format('d.m.Y')}}</td>
                        <td>{{$invoice->invoice_type}}</td>
                        <td>@money($invoice->invoiceLists->sum('total_price')) Kč</td>
                        <td class="d-flex justify-content-center">
                            <a href="#" class="d-inline-block mr-1"><img src="{{asset('svg/email.svg')}}" class="card-body__action-icon" alt=""></a>
                            <a href="{{route('invoiceDownload', $invoice->id)}}" class="d-inline-block mr-1"><img src="{{asset('svg/download.svg')}}" class="card-body__action-icon" alt=""></a>
                            <a href="{{route('invoiceCopy', $invoice->id)}}" class="d-inline-block mr-1"><img src="{{asset('svg/copy.svg')}}" class="card-body__action-icon" alt=""></a>
                            <a href="{{route('invoiceEdit', $invoice->id)}}" class="d-inline-block mr-1"><img src="{{asset('svg/edit.svg')}}" class="card-body__action-icon" alt=""></a>
                            <a href="#" data-toggle="modal" data-target="#deleteModal-{{$key}}" class="d-inline-block"><img src="{{asset('svg/delete-black.svg')}}" class="card-body__action-icon" alt=""></a>
                        </td>
                    </tr>
                    <!-- Modal -->
                    <div class="modal fade" id="deleteModal-{{$key}}" tabindex="-1" role="dialog"  aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Odstranit fakturu</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                   Opravdu chcete odstranit fakturu?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Ne</button>
                                    <a href="{{route('invoiceDelete', $invoice->id)}}"><button type="button" class="btn btn-danger">Ano</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="d-flex justify-content-center">
            <div class="pagination">
                {{$invoices->links()}}
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            let mainCheckBox = document.querySelector("#invoice-id");
            let invoiceCheckboxes = document.querySelectorAll(".invoice-checkbox");
            mainCheckBox.addEventListener('click', () => {
                if(mainCheckBox.checked === true) {
                    invoiceCheckboxes.forEach(element => {
                        if(element.checked === true) {
                            return;
                        }
                        element.checked = true
                    })
                } else {
                    invoiceCheckboxes.forEach(element => {
                        if(element.checked === false) {
                            return;
                        }
                        element.checked = false
                    })
                }
            })
        });
    </script>
@endsection
