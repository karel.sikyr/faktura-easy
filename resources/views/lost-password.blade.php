@extends('layouts.page')

@section('title', '| Reset hesla')

@section('content')

    <div class="container">
        <div class="text-center mb-4 mt-4">
            <h3 class="font-weight-bolder text-uppercase">Reset hesla</h3>
        </div>
        <div class="card mb-4 border-0 w-50 mr-auto ml-auto">
            <div class="card-body">
                <form method="POST" action="">
                    @csrf
                    {!! Honeypot::generate('my_name', 'my_time') !!}
                    <p>V případě že jste zapoměli heslo, zadejte váš e-mail. Zašleme Vám odkaz na jeho resetování.</p>
                    <div class="form-group">
                        <label for="loginEmail">* E-mail</label>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror"
                               id="loginEmail" aria-describedby="emailHelp" value="{{ old('email') }}" required
                               autocomplete="email" autofocus>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Odeslat</button>
                </form>
            </div>

            {{-- nastavení nového hesla --}}
            <div class="card-body">
                <p>Zadejte své nové heslo</p>
                <form method="POST" action="">
                    @csrf
                    {!! Honeypot::generate('my_name', 'my_time') !!}
                    <div class="form-group">
                        <label for="registrationPassword">* Heslo</label>
                        <input type="password" name="password"
                               class="form-control @error('password') is-invalid @enderror" id="registrationPassword"
                               required autocomplete="new-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="registrationPasswordForCheck">* Heslo pro kontrolu</label>
                        <input type="password" name="password_confirmation" class="form-control"
                               id="registrationPasswordForCheck" required autocomplete="new-password">
                    </div>
                    <button type="submit" class="btn btn-primary">Odeslat</button>
                </form>
            </div>
        </div>
    </div>

@endsection
