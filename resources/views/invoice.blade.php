@extends('layouts.page')

@section('title', '| Faktura')

@section('content')

    <div id="app" v-cloak class="container">
        <div class="text-center mb-4 mt-4">
            <h3 class="font-weight-bolder text-uppercase mb-0">
                <span v-if="invoiceData && invoiceData.type == 'edit'">Úprava faktury: @{{ invoiceNumber }}</span>
                <span v-else>Vystavení nové faktury</span>
            </h3>
        </div>

        @include('includes.invoice-alert')

        <div class="card mb-4 border-0">
            <div class="card-body">
                {!! Form::open(['method'=>'POST', 'class'=>'row', 'autocomplete' => 'off', 'action' => 'App\Http\Controllers\InvoiceController@sendInvoice']) !!}
                <div class="col-md-6 mb-4">
                    <div class="heading-with-image">
                        <img src="{{asset('svg/invoice.svg')}}" class="img-fluid" alt="Faktura">
                        <h3 class="font-weight-bolder text-uppercase">Faktura</h3>
                    </div>
                    <input type="hidden" name="invoice_type" value="{{$invoiceData['type']}}">
                    <input type="hidden" name="invoice_id" value="{{$invoiceData['id']}}">
                    <div class="form-group form-group--custom">
                        <label for="invoiceType">Druh faktury</label>
                        <select name="invoiceType" id="invoiceType" v-model="invoiceType" class="form-control">
                            @foreach(\App\Models\Invoice::INVOICE_TYPES as $key => $value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="invoiceNumber">* Číslo</label>
                            <input name="invoiceNumber" v-model="invoiceNumber" type="text" class="form-control" id="invoiceNumber" required>
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="invoiceEvidentNumber">* Evidenční číslo</label>
                            <input name="invoiceEvidentNumber" v-model="invoiceEvidentNumber" type="text" class="form-control" required
                                   id="invoiceEvidentNumber">
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="invoiceCreatedDate">* Datum vystavení</label>
                        <vue-flat-pickr name="invoiceCreatedDate" class="form-control bg-white pointer" v-model="invoiceCreatedDate"
                                        :config="dateTimeConfig" required></vue-flat-pickr>
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="invoiceDueDateType">* Splatnost</label>
                        <select name="invoiceDueDateType" v-model="invoiceDueDateType" id="invoiceDueDateType" class="form-control" required>
                            @foreach(\App\Models\Invoice::DUE_DATE_TYPES as $key => $value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div v-if="invoiceDueDateType == 6 " class="form-group form-group--custom">
                        <label for="invoiceDueDate-different">Splatnost datum</label>
                        <vue-flat-pickr v-model="invoiceDueDate" id="invoiceDueDate-different" name="invoiceDueDate" class="form-control bg-white pointer"
                                        :config="dateTimeConfig"></vue-flat-pickr>
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="madeBy">* Vystavil</label>
                        <input name="madeBy" v-model="madeBy" type="text" class="form-control" id="madeBy" required>
                    </div>
                </div>
                <div class="col-md-6 mb-4">
                    <div class="heading-with-image">

                    </div>
                    <div class="form-group form-group--custom">
                        <label for="currency">* Měna</label>
                        <select name="currency" id="currency" class="form-control" required>
                            @foreach($currencies as $id => $currency)
                                <option value="{{$id}}">{{$currency}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="formOfPayment">* Forma úhrady</label>
                        <select name="formOfPayment" id="formOfPayment" class="form-control" required>
                            @foreach(\App\Models\Invoice::PAYMENT_TYPES as $key => $value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="accountNumber">* Číslo účtu</label>
                        <input name="accountNumber" v-model="accountNumber" type="text" class="form-control" id="accountNumber" required>
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="variableSymbol">Variabilní symbol</label>
                        <input name="variableSymbol" v-model="variableSymbol" type="number" class="form-control" id="variableSymbol">
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="accountIban">IBAN</label>
                        <input name="accountIban" v-model="accountIban" type="text" class="form-control" id="accountIban">
                    </div>

                    {{-- Logo/razíko atp. --}}
                    <div>

                        {{--                        <div class="form-group form-group--custom">--}}
                        {{--                            <label for="rounding">Zaokrouhlení</label>--}}
                        {{--                            <select v-model="rounding" id="rounding" class="form-control">--}}
                        {{--                                <option value="Žádné" selected>Žádné</option>--}}
                        {{--                                <option value="Nejbližších 10 haleřů">Nejbližších 10 haleřů</option>--}}
                        {{--                                <option value="Celá čísla">Celá čísla</option>--}}
                        {{--                            </select>--}}
                        {{--                        </div>--}}
                        {{--                        <div class="form-group form-group--custom">--}}
                        {{--                            <label for="language">Jazyk faktury</label>--}}
                        {{--                            <select v-model="language" id="language" class="form-control">--}}
                        {{--                                <option value="cz" selected>Česky</option>--}}
                        {{--                                <option value="aj">Anglicky</option>--}}
                        {{--                            </select>--}}
                        {{--                        </div>--}}
                        {{--                        <div class="form-group form-group--custom">--}}
                        {{--                            <div class="input-group mb-3">--}}
                        {{--                                <div class="input-group-prepend">--}}
                        {{--                                    <span class="input-group-text" id="fileLogo">Logo</span>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="custom-file">--}}
                        {{--                                    <input v-model="fileLogo" type="file" class="custom-file-input" id="fileLogo"--}}
                        {{--                                           aria-describedby="fileLogo">--}}
                        {{--                                    <label class="custom-file-label" for="fileLogo">Vybrat soubor</label>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        {{--                        <div class="form-group form-group--custom">--}}
                        {{--                            <div class="input-group mb-3">--}}
                        {{--                                <div class="input-group-prepend">--}}
                        {{--                                    <span class="input-group-text" id="fileMark">Razítko</span>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="custom-file">--}}
                        {{--                                    <input v-model="fileMark" type="file" class="custom-file-input" id="fileMark"--}}
                        {{--                                           aria-describedby="fileMark">--}}
                        {{--                                    <label class="custom-file-label" for="fileMark">Vybrat soubor</label>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        {{--                        <div class="form-group form-group__invoiceType">--}}
                        {{--                            <div class="d-flex">--}}
                        {{--                                <div class="d-flex flex-column align-items-center mr-3 pointer">--}}
                        {{--                                    <label class="d-flex flex-column align-items-center" for="invoiceType-1">--}}
                        {{--                                        <h5 class="font-weight-bolder text-uppercase">jednoduchý</h5>--}}
                        {{--                                        <img--}}
                        {{--                                            src="https://www.fakturaonline.cz/images/invoice-appearance/medium-modern.png"--}}
                        {{--                                            alt="invoiceType-1">--}}
                        {{--                                    </label>--}}
                        {{--                                    <input v-model="invoiceType" value="1" id="invoiceType-1" type="radio" name="invoiceType" checked>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="d-flex flex-column align-items-center mr-3 pointer">--}}
                        {{--                                    <label class="d-flex flex-column align-items-center" for="invoiceType-2">--}}
                        {{--                                        <h5 class="font-weight-bolder text-uppercase">moderní</h5>--}}
                        {{--                                        <img--}}
                        {{--                                            src="https://www.fakturaonline.cz/images/invoice-appearance/medium-modern.png"--}}
                        {{--                                            alt="invoiceType-2">--}}
                        {{--                                    </label>--}}
                        {{--                                    <input v-model="invoiceType" value="2" id="invoiceType-2" type="radio" name="invoiceType">--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                    </div>
                    {{-- Logo/razíko atp. --}}
                </div>
                <div class="col-md-6 mb-4">
                    <div class="heading-with-image">
                        <img src="{{asset('svg/user.svg')}}" class="img-fluid" alt="Faktura">
                        <h3 class="font-weight-bolder text-uppercase">ODBĚRATEL</h3>
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="companyHolder">
                            Odběratel faktury
                        </label>
                            <v-select
                                class="w-100"
                                id="companyHolder"
                                v-model="companyHolder"
                                @input="companyHolderChange"
                                label="name"
                                :options="subscribers">
                                <template slot="no-options">
                                </template>
                                <template slot="option" slot-scope="option">
                                    <div class="d-center">
                                        @{{ option.name }}
                                    </div>
                                </template>
                                <template slot="selected-option" slot-scope="option">
                                    <div class="selected d-center">
                                        @{{ option.name }}
                                    </div>
                                </template>
                            </v-select>
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="companyCin" class="mb-3">* IČO</label>
                        <div class="w-100">
                            <input name="companyCin" @change="companyCinChange(companyCin)" v-model="companyCin" type="text" class="form-control" id="companyCin" required>
                            <small v-if="companyCinError" id="companyCin" class="form-text text-muted">@{{ companyCinError }}</small>
                        </div>
                    </div>

                    <div class="form-group form-group--custom">
                        <label for="companyName">* Jméno</label>
                        <input name="companyName" v-model="companyName" type="text" class="form-control" id="companyName" required>
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="companyTin">DIČ</label>
                        <input name="companyTin" v-model="companyTin" type="text" class="form-control" id="companyTin">
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="companyStreet">Ulice</label>
                        <input name="companyStreet" v-model="companyStreet" type="text" class="form-control" id="companyStreet">
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="companyCity">Město</label>
                        <input name="companyCity" v-model="companyCity" type="text" class="form-control" id="companyCity">
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="companyZip">PSČ</label>
                        <input name="companyZip" v-model="companyZip" type="text" class="form-control" id="companyZip">
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="companyState">Země</label>
                        <select name="companyState" v-model="companyState" id="companyState" class="form-control">
                            @foreach($countries as $id => $country)
                                <option value="{{$id}}">{{$country}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6 mb-4">
                    <div class="heading-with-image">
                        <img src="{{asset('svg/user.svg')}}" class="img-fluid" alt="Faktura">
                        <h3 class="font-weight-bolder text-uppercase">Dodavatel</h3>
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="cin" class="mb-3">* IČO</label>
                        <div class="w-100">
                            <input name="cin" v-model="cin" @change="cinChange(cin)" type="text" class="form-control" id="cin" required>
                            <small v-if="cinError" id="cin" class="form-text text-muted">@{{ cinError }}</small>
                        </div>
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="name">* Jméno</label>
                        <input name="name" v-model="name" type="text" class="form-control" id="name" required>
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="tin">DIČ</label>
                        <input name="tin" v-model="tin" type="text" class="form-control" id="tin">
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="street">Ulice</label>
                        <input name="street" v-model="street" type="text" class="form-control" id="street">
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="city">Město</label>
                        <input name="city" v-model="city" type="text" class="form-control" id="city">
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="zip">PSČ</label>
                        <input name="zip" v-model="zip" type="text" class="form-control" id="zip">
                    </div>
                </div>
                <input type="hidden" name="generateInvoice" v-model="JSON.stringify(items)">
                <div class="col-md-12 mb-4">
                    <div class="heading-with-image">
                        <img src="{{asset('svg/note.svg')}}" class="img-fluid" alt="Faktura">
                        <h3 class="font-weight-bolder text-uppercase">POZNÁMKA</h3>
                    </div>
                    <div class="form-group form-group--custom">
                        <label class="w-100">
                                <textarea name="note" v-model="note" class="form-control" id="" cols="30"
                                          rows="4"></textarea>
                        </label>
                    </div>
                    <div class="form-group form-group--custom">
                        <label for="registerInformation" class="w-50">Informace o zapsání do rejstříku</label>
                        <input name="registerInformation" v-model="registerInformation" type="text" class="form-control"
                               id="registerInformation">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="heading-with-image">
                        <img src="{{asset('svg/invoice-list.svg')}}" class="img-fluid" alt="Faktura">
                        <h3 class="font-weight-bolder text-uppercase">POLOŽKY FAKTURY</h3>
                    </div>
                    <div class="generateInvoice generateInvoice--header">
                        <div class="generateInvoice__counter">
                            Počet
                        </div>
                        <div class="generateInvoice__counter">
                            M.J.
                        </div>
                        <div class="generateInvoice__text">
                            Popis
                        </div>
                        <div class="generateInvoice__vat">
                            Sazba DPH
                        </div>
                        <div class="generateInvoice__price">
                            Cena (Kč)
                        </div>
                        <div class="generateInvoice__price">
                            Celkem (Kč)
                        </div>
                        <div class="generateInvoice__action mr-0">
                            Akce
                        </div>
                    </div>
                    <div v-for="item in items" class="generateInvoice">
                        <div class="generateInvoice__counter">
                            <input id="generateInvoiceCounter" @change="changeCounterAndPrice(item)" v-model="item.counter" type="number" class="form-control">
                        </div>
                        <div class="generateInvoice__counter">
                            <input id="generateInvoiceCounterNaming" v-model="item.pc" type="text" value="Ks"  class="form-control">
                        </div>
                        <div class="generateInvoice__text">
                            <textarea id="generateInvoiceName" v-model="item.note" class="form-control"></textarea>
                        </div>
                        <div class="generateInvoice__vat">
                            <select disabled class="form-control" id="generateInvoiceVat">
                                <option value="0" selected>0 %</option>
                                <option value="21">21 %</option>
                            </select>
                        </div>
                        <div class="generateInvoice__price">
                            <input id="generateInvoicePrice" @change="changeCounterAndPrice(item)" v-model="item.price" type="number" class="form-control">
                        </div>
                        <div class="generateInvoice__price">
                            <input id="generateInvoiceTotalPrice" v-model="item.totalPrice" type="number" disabled class="form-control">
                        </div>
                        <div class="generateInvoice__action mr-0">
                            <a href="#" @click.prevent="removeItem(item)">
                                <img src="{{asset('svg/delete.svg')}}" alt="">
                            </a>
                        </div>
                    </div>
                    <a href="#" @click.prevent="addItem" class="btn btn-primary">Přídat položku</a>
                    <div class="text-right mr-md-4">
                        <h4 class="font-weight-bolder">Celkem bez DPH: @{{ countTotalPrice | splitNumber }} Kč</h4>
                        <h3 class="font-weight-bolder">Celkem s DPH: @{{ countTotalPrice | splitNumber }} Kč</h3>
                    </div>
                </div>
                <div class="col-md-12 mt-4">
                    <div class="d-flex justify-content-between">
                        <div class="invoice-preview">
                            <a href="#">
                                <img src="{{asset('svg/pdf.svg')}}" class="img-fluid" alt="">
                                Náhled faktury</a>
                        </div>
                        <div>
{{--                            <a href="#" class="btn btn-primary">Odeslat e-mailem</a>--}}
                            <a href="#" class="btn btn-primary">Uložit a stáhnout</a>
                            <button type="submit" class="btn btn-primary">Uložit</button>
                        </div>
                    </div>

{{--                    <br>--}}
{{--                    <br>--}}
{{--                    <br>--}}
{{--                    @{{ items }}--}}

                </div>

                {!! Form::close() !!}
            </div>
        </div>

    </div>

@endsection

@include('includes.script')
