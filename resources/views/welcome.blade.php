@extends('layouts.page')

@section('content')

    <div class="banner container-fluid p-0 mb-5 bg-primary">
        <div class="container pt-5 pb-5">
            <div class="row d-md-flex align-items-center">
                <div class="col-md-6 text-white mb-5 mb-md-5">
                    <h1 class="mb-4">Vystavujte faktury <span class="font-weight-bolder">rychle, bezpečně a hlavně zadarmo.</span>
                    </h1>
                    <h5 class="mb-4">Vystavujete faktury dle vzoru v Excelu? Faktura z easy-faktura.cz je mnohem více
                        než jen vzor faktury. Její vystavení je snadné, rychlé a vystavená faktura je ve formátu Adobe
                        PDF - populárním univerzálním dokumentovém formátu. Stačí zadat pár údajů do formuláře a ihned
                        máte připravenou PDF fakturu ke stažení.</h5>
                    <a class="btn btn-success btn-lg" href="{{route('invoiceCreate')}}">
                        <img src="{{asset('svg/plus.svg')}}" class="img-fluid mr-2"
                             alt="easy-faktura.cz - vystavit fakturu">
                        Vystavit fakturu
                    </a>
                </div>
                <div class="col-md-6">
                    <img src="{{asset('svg/bill.svg')}}" class="img-fluid" alt="easy-faktura.cz">
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div class="card border-0">
                    <div class="card-body">
                        <h4>Lorem ipsum dolor sit amet</h4>
                        <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque ducimus et in itaque minima odio
                            provident sit sunt unde. Atque ex non odio recusandae vitae. Consectetur facilis iure quia
                            sit?</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div class="card border-0">
                    <div class="card-body">
                        <h4>Lorem ipsum dolor sit amet</h4>
                        <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque ducimus et in itaque minima odio
                            provident sit sunt unde. Atque ex non odio recusandae vitae. Consectetur facilis iure quia
                            sit?</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4 mb-md-0">
                <div class="card border-0">
                    <div class="card-body">
                        <h4>Lorem ipsum dolor sit amet</h4>
                        <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque ducimus et in itaque minima odio
                            provident sit sunt unde. Atque ex non odio recusandae vitae. Consectetur facilis iure quia
                            sit?</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4 mb-md-0">
                <div class="card border-0">
                    <div class="card-body">
                        <h4>Lorem ipsum dolor sit amet</h4>
                        <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque ducimus et in itaque minima odio
                            provident sit sunt unde. Atque ex non odio recusandae vitae. Consectetur facilis iure quia
                            sit?</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    @if(session()->has('empty-auth'))
        <script>
            $("#login").modal('show')
        </script>
    @endif
@endsection
