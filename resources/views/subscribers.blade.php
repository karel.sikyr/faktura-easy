@extends('layouts.page')

@section('title', '| Odběratelé')

@section('content')

    <div class="container">
        <div class="text-center mb-4 mt-4">
            <h3 class="font-weight-bolder text-uppercase">Odběratelé</h3>
        </div>
        <div class="row">
            <div id="app" class="col-md-5">
                <div class="card-body card mb-4 border-0">
                    <form action="">
                        <div class="form-group form-group--custom">
                            <label for="companyCin" class="mb-3">* IČO</label>
                            <div class="w-100">
                                <input name="companyCin" @change="companyCinChange(companyCin)" v-model="companyCin" type="text" class="form-control" id="companyCin" required>
                                <small v-if="companyCinError" id="companyCin" class="form-text text-muted">@{{ companyCinError }}</small>
                            </div>
                        </div>
                        <div class="form-group form-group--custom">
                            <label for="companyName">* Jméno</label>
                            <input name="companyName" v-model="companyName" type="text" class="form-control" id="companyName" required>
                        </div>
                        <div class="form-group form-group--custom">
                            <label for="companyTin">DIČ</label>
                            <input name="companyTin" v-model="companyTin" type="text" class="form-control" id="companyTin">
                        </div>
                        <div class="form-group form-group--custom">
                            <label for="companyStreet">Ulice</label>
                            <input name="companyStreet" v-model="companyStreet" type="text" class="form-control" id="companyStreet">
                        </div>
                        <div class="form-group form-group--custom">
                            <label for="companyCity">Město</label>
                            <input name="companyCity" v-model="companyCity" type="text" class="form-control" id="companyCity">
                        </div>
                        <div class="form-group form-group--custom">
                            <label for="companyZip">PSČ</label>
                            <input name="companyZip" v-model="companyZip" type="text" class="form-control" id="companyZip">
                        </div>
                        <div class="form-group form-group--custom">
                            <label for="companyState">Země</label>
                            <select name="companyState" v-model="companyState" id="companyState" class="form-control">
                                @foreach($countries as $id => $country)
                                    <option value="{{$id}}">{{$country}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="text-md-right">
                            <button type="submit" class="btn btn-primary">Uložit</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-7">
                <div class="card-body card mb-4 border-0">
                    <table class="table table-striped mb-0">
                        <thead class="thead-dark">
                        <tr class="mb-4">
                            <th scope="col" class="bg-primary border-0">Jméno</th>
                            <th scope="col" class="bg-primary border-0">IČO</th>
                            <th scope="col" class="bg-primary border-0 text-center">Akce</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Webthinx</td>
                            <td>1234567</td>
                            <td class="d-flex justify-content-center">
                                <a href="#" class="d-inline-block mr-1"><img src="{{asset('svg/edit.svg')}}" class="card-body__action-icon" alt=""></a>
                                <a href="#" data-toggle="modal" data-target="#deleteModal-1" class="d-inline-block"><img src="{{asset('svg/delete-black.svg')}}" class="card-body__action-icon" alt=""></a>
                            </td>
                        </tr>
                        <!-- Modal -->
                        <div class="modal fade" id="deleteModal-1" tabindex="-1" role="dialog"  aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Odstranit fakturu</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Opravdu chcete odstranit fakturu?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Ne</button>
                                        <a href="#"><button type="button" class="btn btn-danger">Ano</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </table>
                </div>
            </div>

        </div>
    </div>

@endsection

@include('includes.script')
