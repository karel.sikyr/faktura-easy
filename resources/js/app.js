require('./bootstrap');

import Vue from 'vue';
import VueFlatPickr from 'vue-flatpickr-component';
import 'flatpickr/dist/flatpickr.css';

import vSelect from 'vue-select';
import 'vue-select/dist/vue-select.css';
Vue.component('VSelect', vSelect);
const Czech = require('flatpickr/dist/l10n/cs.js').default.cs;

Vue.filter('splitNumber', function (value) {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
})

if (document.querySelector('#app')) {
    const app = new Vue({
        el: '#app',
        components: {
            VueFlatPickr
        },
        data: {
            invoiceData: window.invoiceData,

            invoiceCreatedDate: '2020-10-16',
            dateTimeConfig: {
                wrap: true,
                enableTime: false,
                dateFormat: 'Y-m-d',
                altInput: true,
                locale: Czech
            },

            invoiceNumber: null,
            invoiceEvidentNumber: null,
            invoiceDueDateType: 4,
            invoiceDueDate: null,
            // formOfPayment: null,
            variableSymbol: null,
            madeBy: null,

            // currency: null,
            rounding: null,
            language: null,
            fileLogo: null,
            fileMark: null,
            invoiceType: 1,

            companyHolder: null,
            companyName: null,
            companyTin: null,
            companyCin: null,
            companyStreet: null,
            companyCity: null,
            companyZip: null,
            companyState: 28,

            cin: null,
            name: null,
            tin: null,
            street: null,
            city: null,
            zip: null,

            accountNumber: null,
            accountIban: null,

            note: null,
            registerInformation: null,
            items: [],
            companyCinError: 'Vyplněním tohoto políčka dostanete automaticky zbytek informací.',
            cinError: 'Vyplněním tohoto políčka dostanete automaticky zbytek informací.',

            subscribers: window.subscribers,
            getDataOfPersonByApi: window.getDataOfPersonByApi,
            getInvoiceDate: window.getInvoiceDate,
            getSubscriberData: window.getSubscriberData,
        },
        methods: {
            callApi(ico, company){
                if(!ico || ico === '') {
                    return;
                }
                var vm = this;
                axios.get(vm.getDataOfPersonByApi, {
                    params: {
                        ico: ico,
                    }
                })
                    .then(function (response) {
                        console.log(response);
                        if(response.data.length === 0) {
                            return;
                        }
                        if(company) {
                            vm.fillCompanyInformation(response.data);
                            return;
                        }
                        vm.fillYourInformation(response.data);
                    })
                    .catch(function (error) {
                        console.log(error);
                    })
            },
            companyCinChange(ico) {
                this.callApi(ico, true)
            },
            cinChange(ico) {
                this.callApi(ico, false)
            },
            fillCompanyInformation(response) {
                this.companyCinError = null;
                if(!response || response.error) {
                   this.companyCinError = 'Toto IČO je neplatné.';
                   return ;
                }

                let mapping = {
                    'companyCin': 'ico',
                    'companyName': 'name',
                    'companyStreet': 'street',
                    'companyCity': 'city',
                    'companyTin': 'dic',
                    'companyZip': 'postcode',
                    'companyState': 'country_id',
                }

                for (const [key, value] of Object.entries(mapping)) {
                    if(!value) { continue }
                    this.$data[key] = response[value]
                }
            },
            fillYourInformation(response) {
                this.cinError = null;

                if(!response || response.error) {
                    this.cinError = 'Toto IČO je neplatné.';
                    return ;
                }

                let mapping = {
                    'name': 'name',
                    'tin': 'street',
                    'city': 'city',
                    'zip': 'zip',
                    'street': 'street',
                }

                for (const [key, value] of Object.entries(mapping)) {
                    if(!value) { continue }
                    this.$data[key] = response[value]
                }
            },
            changeCounterAndPrice(item) {
                item.price = parseInt(item.price);
                item.counter = parseInt(item.counter);
                item.totalPrice = item.price * item.counter;
            },
            removeItem(item) {
                this.items.splice(item, 1)
            },
            addItem(itemFromResponse = null) {
                let item;

                if (!itemFromResponse) {
                    item = {
                        counter: 1,
                        pc: 'Ks',
                        note: '',
                        price: 0,
                        totalPrice: 0
                    }
                } else {
                    item = {
                        counter: itemFromResponse.counter ? itemFromResponse.counter : 1,
                        pc: itemFromResponse.pc ? itemFromResponse.pc : 'Ks',
                        note: itemFromResponse.note ? itemFromResponse.note : '',
                        price: itemFromResponse.price ? itemFromResponse.price : 0,
                        totalPrice: itemFromResponse.total_price ? itemFromResponse.total_price : 0
                    }
                }

                this.items.push(item)
            },

            getInvoiceData(){
                var vm = this;
                axios.post(vm.getInvoiceDate, {
                        invoiceId: vm.invoiceData.id,
                        invoiceType: vm.invoiceData.type
                    })
                    .then(function (response) {
                        console.log(response);
                        if(response.data.length === 0) {
                            return;
                        }
                        vm.setInvoiceResponseData(response.data);
                        vm.fillCompanyInformation(response.data.invoice_subscriber);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },

            setInvoiceResponseData(response){
                let mapping = {
                    'accountNumber': 'bank_account_number',
                    'invoiceCreatedDate': 'date_of_issue',
                    'invoiceDueDateType': 'due_date_id',
                    'invoiceDueDate': 'due_date',
                    'accountIban': 'iban_account_number',
                    'invoiceEvidentNumber': 'evidence_number',
                    'invoiceType': 'type_of_payment',
                    'variableSymbol': 'variable_symbol',
                    'invoiceNumber': 'reference_number',
                    'note': 'note',
                    'registerInformation': 'index_note',
                    'madeBy': 'founder',
                    'name': 'name',
                    'street': 'street',
                    'city': 'city',
                    'tin': 'dic',
                    'zip': 'postcode',
                    'cin' : 'ico'
                }

                let invoiceList = [];
                if(response.invoice_lists.lenght !== 0) {
                    invoiceList = response.invoice_lists
                    invoiceList.forEach((item)=> {
                        this.addItem(item)
                    })
                }

                for (const [key, value] of Object.entries(mapping)) {
                    if(!value) { continue }
                    this.$data[key] = response[value]
                }
            },

            companyHolderChange() {
                if(!this.companyHolder) {
                    return;
                }

                var vm = this;
                axios.post(vm.getSubscriberData, {
                    id: vm.companyHolder.id,
                })
                .then(function (response) {
                    console.log(response);
                    if(response.data.length === 0) {
                        return;
                    }
                    vm.fillCompanyInformation(response.data);
                })
                .catch(function (error) {
                    console.log(error);
                });
            },
        },
        created() {
            this.getInvoiceData()

            if(this.invoiceData && invoiceData.type == 'create') {
                this.addItem()
            }

            let date = new Date();
            this.invoiceCreatedDate = date;
            this.invoiceDueDate = date;
        },
        watch: {
            invoiceNumber: function (value) {
                this.invoiceNumber = value;
                this.invoiceEvidentNumber = value;
            },
        },
        computed: {
            countTotalPrice(){
                return this.items.reduce(function (sum, item) {
                    return sum + item.totalPrice;
                }, 0)
            },
        }

    });
}




