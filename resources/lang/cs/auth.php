<?php

return [

    'failed' => 'Tyto přihlašovací údaje neodpovídají našim záznamům.',
    'password' => 'Zadané heslo je nesprávné.',
    'throttle' => 'Příliš mnoho pokusů o přihlášení. Zkuste to znovu za :seconds sekund.',

];
