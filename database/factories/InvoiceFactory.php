<?php

namespace Database\Factories;

use App\Models\Invoice;
use App\Models\InvoiceCurrency;
use App\Models\InvoiceSubscriber;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class InvoiceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Invoice::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => 1, // @TODO adam
            'type_id' => Arr::random(array_flip(Invoice::INVOICE_TYPES)),
            'design_type_id' => 1,
            'rounding_type_id' => 1,
            'invoice_currency_id' => InvoiceCurrency::query()->inRandomOrder()->first()->id,
            'invoice_language_id' => 1,
            'invoice_subscriber_id' => InvoiceSubscriber::factory()->create()->id,
            'reference_number' => random_int(100000, 999999),
            'evidence_number' => random_int(100000, 999999),
            'date_of_issue' => Carbon::now()->subDays(random_int(1, 5)),
            'due_date_id' => 6,
            'due_date' => Carbon::now()->addDays(random_int(10, 15)),
            'type_of_payment' => Arr::random(array_flip(Invoice::PAYMENT_TYPES)),
            'variable_symbol' => random_int(100000, 999999),
            'founder' => $this->faker->name,
            'name' => $this->faker->name,
            'ico' => random_int(100000, 999999),
            'dic' => random_int(100000, 999999),
            'street' => $this->faker->streetName,
            'city' => $this->faker->city,
            'postcode' => $this->faker->postcode,
            'bank_account_number' => random_int(100000, 999999) . '/' . random_int(1000, 9999),
            'iban_account_number' => random_int(100000, 999999),
            'note' => $this->faker->text(50),
            'index_note' => $this->faker->text(50),
        ];
    }
}
