<?php

namespace Database\Factories;

use App\Models\Country;
use App\Models\InvoiceSubscriber;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvoiceSubscriberFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = InvoiceSubscriber::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => 1, // @TODO adam
            'name' => $this->faker->name,
            'ico' => random_int(100000, 999999),
            'dic' => random_int(100000, 999999),
            'street' => $this->faker->streetName,
            'city' => $this->faker->city,
            'postcode' => $this->faker->postcode,
            'country_id' => Country::query()->inRandomOrder()->first()->id,
        ];
    }
}
