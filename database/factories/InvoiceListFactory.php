<?php

namespace Database\Factories;

use App\Models\Invoice;
use App\Models\InvoiceList;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvoiceListFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = InvoiceList::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'invoice_id' => Invoice::query()->inRandomOrder()->first()->id,
            'counter' => $counter = random_int(1, 4),
            'pc' => 'Ks',
            'note' => $this->faker->sentence(2),
            'price' => $price = random_int(1000, 20000),
            'total_price' => $price * $counter
        ];
    }
}
