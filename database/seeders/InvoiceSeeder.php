<?php

namespace Database\Seeders;

use App\Jobs\GenerateInvoice;
use App\Models\Invoice;
use App\Models\InvoiceList;
use Illuminate\Database\Seeder;

class InvoiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++){
            $invoice = Invoice::factory()->create();

            for ($j = 0; $j < random_int(1, 5); $j++){
                InvoiceList::factory()->create([
                    'invoice_id' => $invoice->id
                ]);
            }

            dispatch(new GenerateInvoice($invoice));
        }
    }
}
