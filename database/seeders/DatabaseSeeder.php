<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if(!App::environment('production')){
            $this->call(UserSeeder::class);
        }

        $this->call(CountrySeeder::class);
        $this->call(InvoiceCurrencySeeder::class);
        $this->call(InvoiceLanguageSeeder::class);

        if(!App::environment('production')){
            $this->call(InvoiceSeeder::class);
        }
    }
}
