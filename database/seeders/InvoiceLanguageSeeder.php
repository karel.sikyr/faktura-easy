<?php

namespace Database\Seeders;

use App\Models\InvoiceLanguage;
use Illuminate\Database\Seeder;

class InvoiceLanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'name' => 'Česky'
            ],
            [
                'name' => 'Anglicky'
            ],
        ];

        InvoiceLanguage::query()->insert($items);
    }
}
