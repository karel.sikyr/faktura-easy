<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::query()->insert($this->getData());
    }

    /**
     * @return array
     */
    private function getData(): array
    {
        return [
            [
                'name' => 'Afghánská islámská republika'
            ],
            [
                'name' => 'Albánská republika'
            ],
            [
                'name' => 'Alžírská demokratická a lidová republika'
            ],
            [
                'name' => 'Andorrské knížectví'
            ],
            [
                'name' => 'Angolská republika'
            ],
            [
                'name' => 'Antigua a Barbuda'
            ],
            [
                'name' => 'Argentinská republika'
            ],
            [
                'name' => 'Arménská republika'
            ],
            [
                'name' => 'Australské společenství'
            ],
            [
                'name' => 'Ázerbájdžánská republika'
            ],
            [
                'name' => 'Bahamské společenství'
            ],
            [
                'name' => 'Království Bahrajn'
            ],
            [
                'name' => 'Bangladéšská lidová republika'
            ],
            [
                'name' => 'Barbados'
            ],
            [
                'name' => 'Belgické království'
            ],
            [
                'name' => 'Belize'
            ],
            [
                'name' => 'Běloruská republika'
            ],
            [
                'name' => 'Beninská republika'
            ],
            [
                'name' => 'Bhútánské království'
            ],
            [
                'name' => 'Mnohonárodnostní stát Bolívie'
            ],
            [
                'name' => 'Bosna a Hercegovina'
            ],
            [
                'name' => 'Botswanská republika'
            ],
            [
                'name' => 'Brazilská federativní republika'
            ],
            [
                'name' => 'Brunej Darussalam'
            ],
            [
                'name' => 'Bulharská republika'
            ],
            [
                'name' => 'Burkina Faso'
            ],
            [
                'name' => 'Burundská republika'
            ],
            [
                'name' => 'Čadská republika'
            ],
            [
                'name' => 'Černá Hora'
            ],
            [
                'name' => 'Česká republika'
            ],
            [
                'name' => 'Čínská lidová republika'
            ],
            [
                'name' => 'Dánské království'
            ],
            [
                'name' => 'Dominické společenství'
            ],
            [
                'name' => 'Dominikánská republika'
            ],
            [
                'name' => 'Džibutská republika'
            ],
            [
                'name' => 'Egyptská arabská republika'
            ],
            [
                'name' => 'Ekvádorská republika'
            ],
            [
                'name' => 'Stát Eritrea'
            ],
            [
                'name' => 'Estonská republika'
            ],
            [
                'name' => 'Etiopská federativní demokratická republika'
            ],
            [
                'name' => 'Fidžijská republika'
            ],
            [
                'name' => 'Filipínská republika'
            ],
            [
                'name' => 'Finská republika'
            ],
            [
                'name' => 'Francouzská republika'
            ],
            [
                'name' => 'Gabonská republika'
            ],
            [
                'name' => 'Gambijská republika'
            ],
            [
                'name' => 'Ghanská republika'
            ],
            [
                'name' => 'Grenada'
            ],
            [
                'name' => 'Gruzie'
            ],
            [
                'name' => 'Guatemalská republika'
            ],
            [
                'name' => 'Guinejská republika'
            ],
            [
                'name' => 'Republika Guinea-Bissau'
            ],
            [
                'name' => 'Guyanská kooperativní republika'
            ],
            [
                'name' => 'Republika Haiti'
            ],
            [
                'name' => 'Honduraská republika'
            ],
            [
                'name' => 'Chilská republika'
            ],
            [
                'name' => 'Chorvatská republika'
            ],
            [
                'name' => 'Indická republika'
            ],
            [
                'name' => 'Indonéská republika'
            ],
            [
                'name' => 'Irácká republika'
            ],
            [
                'name' => 'Íránská islámská republika'
            ],
            [
                'name' => 'Irsko'
            ],
            [
                'name' => 'Islandská republika'
            ],
            [
                'name' => 'Italská republika'
            ],
            [
                'name' => 'Stát Izrael'
            ],
            [
                'name' => 'Jamajka'
            ],
            [
                'name' => 'Japonsko'
            ],
            [
                'name' => 'Jemenská republika'
            ],
            [
                'name' => 'Jihoafrická republika'
            ],
            [
                'name' => 'Korejská republika'
            ],
            [
                'name' => 'Jihosúdánská republika'
            ],
            [
                'name' => 'Jordánské hášimovské království'
            ],
            [
                'name' => 'Kambodžské království'
            ],
            [
                'name' => 'Kamerunská republika'
            ],
            [
                'name' => 'Kanada'
            ],
            [
                'name' => 'Kapverdská republika'
            ],
            [
                'name' => 'Stát Katar'
            ],
            [
                'name' => 'Republika Kazachstán'
            ],
            [
                'name' => 'Keňská republika'
            ],
            [
                'name' => 'Republika Kiribati'
            ],
            [
                'name' => 'Kolumbijská republika'
            ],
            [
                'name' => 'Komorský svaz'
            ],
            [
                'name' => 'Konžská republika'
            ],
            [
                'name' => 'Konžská demokratická republika'
            ],
            [
                'name' => 'Kostarická republika'
            ],
            [
                'name' => 'Kubánská republika'
            ],
            [
                'name' => 'Stát Kuvajt'
            ],
            [
                'name' => 'Kyperská republika'
            ],
            [
                'name' => 'Kyrgyzská republika'
            ],
            [
                'name' => 'Laoská lidově demokratická republika'
            ],
            [
                'name' => 'Lesothské království'
            ],
            [
                'name' => 'Libanonská republika'
            ],
            [
                'name' => 'Liberijská republika'
            ],
            [
                'name' => 'Libyjský stát'
            ],
            [
                'name' => 'Lichtenštejnské knížectví'
            ],
            [
                'name' => 'Litevská republika'
            ],
            [
                'name' => 'Lotyšská republika'
            ],
            [
                'name' => 'Lucemburské velkovévodství'
            ],
            [
                'name' => 'Madagaskarská republika'
            ],
            [
                'name' => 'Maďarsko'
            ],
            [
                'name' => 'Malajsie'
            ],
            [
                'name' => 'Malawiská republika'
            ],
            [
                'name' => 'Maledivská republika'
            ],
            [
                'name' => 'Republika Mali'
            ],
            [
                'name' => 'Maltská republika'
            ],
            [
                'name' => 'Marocké království'
            ],
            [
                'name' => 'Republika Marshallovy ostrovy'
            ],
            [
                'name' => 'Mauricijská republika'
            ],
            [
                'name' => 'Mauritánská islámská republika'
            ],
            [
                'name' => 'Spojené státy mexické'
            ],
            [
                'name' => 'Federativní státy Mikronésie'
            ],
            [
                'name' => 'Moldavská republika'
            ],
            [
                'name' => 'Monacké knížectví'
            ],
            [
                'name' => 'Mongolsko'
            ],
            [
                'name' => 'Mosambická republika'
            ],
            [
                'name' => 'Republika Myanmarský svaz'
            ],
            [
                'name' => 'Namibijská republika'
            ],
            [
                'name' => 'Republika Nauru'
            ],
            [
                'name' => 'Spolková republika Německo'
            ],
            [
                'name' => 'Nepálská federativní demokratická republika'
            ],
            [
                'name' => 'Nigerská republika'
            ],
            [
                'name' => 'Nigerijská federativní republika'
            ],
            [
                'name' => 'Nikaragujská republika'
            ],
            [
                'name' => 'Nizozemské království'
            ],
            [
                'name' => 'Norské království'
            ],
            [
                'name' => 'Nový Zéland'
            ],
            [
                'name' => 'Sultanát Omán'
            ],
            [
                'name' => 'Pákistánská islámská republika'
            ],
            [
                'name' => 'Republika Palau'
            ],
            [
                'name' => 'Stát Palestina'
            ],
            [
                'name' => 'Panamská republika'
            ],
            [
                'name' => 'Nezávislý stát Papua Nová Guinea'
            ],
            [
                'name' => 'Paraguayská republika'
            ],
            [
                'name' => 'Peruánská republika'
            ],
            [
                'name' => 'Republika Pobřeží slonoviny'
            ],
            [
                'name' => 'Polská republika'
            ],
            [
                'name' => 'Portugalská republika'
            ],
            [
                'name' => 'Rakouská republika'
            ],
            [
                'name' => 'Republika Rovníková Guinea'
            ],
            [
                'name' => 'Rumunsko'
            ],
            [
                'name' => 'Ruská federace'
            ],
            [
                'name' => 'Rwandská republika'
            ],
            [
                'name' => 'Řecká republika'
            ],
            [
                'name' => 'Salvadorská republika'
            ],
            [
                'name' => 'Nezávislý stát Samoa'
            ],
            [
                'name' => 'Republika San Marino'
            ],
            [
                'name' => 'Království Saúdská Arábie'
            ],
            [
                'name' => 'Senegalská republika'
            ],
            [
                'name' => 'Korejská lidově demokratická republika'
            ],
            [
                'name' => 'Republika Severní Makedonie'
            ],
            [
                'name' => 'Seychelská republika'
            ],
            [
                'name' => 'Republika Sierra Leone'
            ],
            [
                'name' => 'Singapurská republika'
            ],
            [
                'name' => 'Slovenská republika'
            ],
            [
                'name' => 'Slovinská republika'
            ],
            [
                'name' => 'Somálská federativní republika'
            ],
            [
                'name' => 'Spojené arabské emiráty'
            ],
            [
                'name' => 'Spojené království Velké Británie a Severního Irska'
            ],
            [
                'name' => 'Spojené státy americké'
            ],
            [
                'name' => 'Srbská republika'
            ],
            [
                'name' => 'Srílanská demokratická socialistická republika'
            ],
            [
                'name' => 'Středoafrická republika'
            ],
            [
                'name' => 'Súdánská republika'
            ],
            [
                'name' => 'Surinamská republika'
            ],
            [
                'name' => 'Svatá Lucie'
            ],
            [
                'name' => 'Federace Svatý Kryštof a Nevis'
            ],
            [
                'name' => 'Demokratická republika Svatý Tomáš a Princův ostrov'
            ],
            [
                'name' => 'Svatý Vincenc a Grenadiny'
            ],
            [
                'name' => 'Svazijské království'
            ],
            [
                'name' => 'Syrská arabská republika'
            ],
            [
                'name' => 'Šalomounovy ostrovy'
            ],
            [
                'name' => 'Španělské království'
            ],
            [
                'name' => 'Švédské království'
            ],
            [
                'name' => 'Švýcarská konfederace'
            ],
            [
                'name' => 'Republika Tádžikistán'
            ],
            [
                'name' => 'Tanzanská sjednocená republika'
            ],
            [
                'name' => 'Thajské království'
            ],
            [
                'name' => 'Tožská republika'
            ],
            [
                'name' => 'Království Tonga'
            ],
            [
                'name' => 'Republika Trinidad a Tobago'
            ],
            [
                'name' => 'Tuniská republika'
            ],
            [
                'name' => 'Turecká republika'
            ],
            [
                'name' => 'Turkmenistán'
            ],
            [
                'name' => 'Tuvalu'
            ],
            [
                'name' => 'Ugandská republika'
            ],
            [
                'name' => 'Ukrajina'
            ],
            [
                'name' => 'Uruguayská východní republika'
            ],
            [
                'name' => 'Republika Uzbekistán'
            ],
            [
                'name' => 'Republika Vanuatu'
            ],
            [
                'name' => 'Městský stát Vatikán'
            ],
            [
                'name' => 'Bolívarovská republika Venezuela'
            ],
            [
                'name' => 'Vietnamská socialistická republika'
            ],
            [
                'name' => 'Demokratická republika Východní Timor'
            ],
            [
                'name' => 'Zambijská republika'
            ],
            [
                'name' => 'Zimbabwská republika'
            ],
        ];
    }
}
