<?php

namespace Database\Seeders;

use App\Models\InvoiceCurrency;
use Illuminate\Database\Seeder;

class InvoiceCurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'name' => 'CZK - Kč (Česká koruna)'
            ],
//            [
//                'name' => 'EUR - € (Euro)'
//            ],
//            [
//                'name' => 'USD - $ (Americký dollar)'
//            ]
        ];

        InvoiceCurrency::query()->insert($items);
    }
}
