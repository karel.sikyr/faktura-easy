<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeIcoColumnToString extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->string('ico')->nullable()->change();
        });

        Schema::table('invoice_subscribers', function (Blueprint $table) {
            $table->string('ico')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->integer('ico')->nullable()->change();
        });

        Schema::table('invoice_subscribers', function (Blueprint $table) {
            $table->integer('ico')->nullable()->change();
        });
    }
}
