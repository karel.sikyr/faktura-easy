<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->unsigned()->index();
            $table->integer('type_id')->unsigned()->index();
            $table->integer('design_type_id')->unsigned()->index();
            $table->integer('rounding_type_id')->unsigned()->index();
            $table->foreignId('invoice_currency_id')->unsigned()->index();
            $table->foreignId('invoice_language_id')->unsigned()->index();
            $table->foreignId('invoice_subscriber_id')->unsigned()->index();

            $table->string('reference_number');
            $table->string('evidence_number');
            $table->date('date_of_issue');
            $table->integer('due_date_id');
            $table->date('due_date');
            $table->integer('type_of_payment');
            $table->integer('variable_symbol');
            $table->string('founder');
            $table->string('order_number')->nullable();

            $table->string('name');
            $table->integer('ico')->nullable();
            $table->string('dic')->nullable();
            $table->string('street')->nullable();
            $table->string('city')->nullable();
            $table->string('postcode')->nullable();
            $table->string('bank_account_number')->nullable();
            $table->string('iban_account_number')->nullable();
            $table->string('note')->nullable();
            $table->string('index_note')->nullable();

            $table->string('logo')->nullable();
            $table->string('stamp')->nullable();

            $table->string('slug');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('invoice_currency_id')->references('id')->on('invoice_currencies');
            $table->foreign('invoice_language_id')->references('id')->on('invoice_languages');
            $table->foreign('invoice_subscriber_id')->references('id')->on('invoice_subscribers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
