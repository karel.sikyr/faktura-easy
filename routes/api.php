<?php

use App\Http\Controllers\Api\IcoApiController;
use App\Http\Controllers\InvoiceController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('get-information-by-cin', [IcoApiController::class, 'index'])->name('get.data-of-person-by-api');
//Route::post('get-invoice-data', [InvoiceController::class, 'getInvoice'])->name('get.invoice-data');
