<?php

use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\Pages\HomePageController;
use App\Http\Controllers\Pages\InvoicePageController;
use App\Http\Controllers\Pages\SettingPageController;
use App\Http\Controllers\Pages\SubscriberPageController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('test', function (){
    return QrCode::generate('Make me into a QrCode!');
});

Route::get('/', [HomePageController::class, 'index'])->name('welcome');
Route::get('/ztracene-heslo', [HomePageController::class, 'lostPassword'])->name('lost-password');
Route::get('/kontakt', [HomePageController::class, 'contact'])->name('contact');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/faktury', [InvoicePageController::class, 'index'])->name('invoiceList');
    Route::get('/faktura', [InvoicePageController::class, 'createInvoice'])->name('invoiceCreate');
    Route::get('/faktura/{id}/edit', [InvoicePageController::class, 'editInvoice'])->name('invoiceEdit');
    Route::get('/faktura/{id}/copy', [InvoicePageController::class, 'copyInvoice'])->name('invoiceCopy');
    Route::get('/faktura/{id}/delete', [InvoicePageController::class, 'destroyInvoice'])->name('invoiceDelete');
    Route::get('/faktura/{id}/download', [InvoicePageController::class, 'downloadInvoice'])->name('invoiceDownload');
    Route::post('/send-invoice', [InvoiceController::class, 'sendInvoice']);

    Route::get('/odberatele', [SubscriberPageController::class, 'index'])->name('subscribers');

    Route::get('/nastaveni', [SettingPageController::class, 'index'])->name('setting');
    Route::post('/nastaveni-update', [SettingPageController::class, 'updateSetting']);

    // @TODO předělat do api
    Route::post('api/get-invoice-data', [InvoiceController::class, 'getInvoice'])->name('get.invoice-data');
    Route::post('api/get-subscriber-data', [InvoiceController::class, 'getSubscriber'])->name('get.subscriber-data');
});

Auth::routes();
