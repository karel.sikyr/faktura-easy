<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'invoiceType' => 'required',
            'currency' => 'required',
            'invoiceNumber' => 'required',
            'invoiceEvidentNumber' => 'required',
            'invoiceCreatedDate' => 'required',
            'invoiceDueDateType' => 'required',
            'formOfPayment' => 'required',
            'variableSymbol' => 'required',
            'name' => 'required',
            'cin' => 'required',
            'street' => 'required',
            'city' => 'required',
            'zip' => 'required',
            'companyName' => 'required',
            'companyCin' => 'required',
            'companyStreet' => 'required',
            'companyCity' => 'required',
            'companyZip' => 'required',
            'companyState' => 'required',
            'accountNumber' => 'required',
            'generateInvoice' => 'required|string'
        ];
    }
}
