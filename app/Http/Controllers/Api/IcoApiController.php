<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use SimpleXMLElement;

class IcoApiController extends Controller
{
    /**
     * @param Request $request
     * @return string[]|null
     *
     * @throws GuzzleException
     */
    public function index(Request $request): ?array
    {
        if(!$ico = $request->ico) {
            return [];
        }

        if(strlen($ico) !== 8){
            return [
              'error' => 'IČO musí být 8 znaků dlouhé.'
            ];
        }

        $xml = $this->getXml($ico);

        $ns = $xml->getDocNamespaces();
        $data = $xml->children($ns['are']);
        $el = $data->children($ns['D'])->VBAS;

        return [
            'ico' => (string) $el->ICO,
            'dic' => (string) $el->DIC,
            'name' => (string) $el->OF,
            'street' => (string) $el->AA->NU . ' ' . (($el->AA->CO == '') ? $el->AA->CD : $el->AA->CD . '/' . $el->AA->CO),
            'city' => (string) $el->AA->N,
            'postcode' => (string) $el->AA->PSC
        ];
    }

    /**
     * @param $ico
     * @return SimpleXMLElement
     *
     * @throws GuzzleException
     */
    private function getXml($ico): SimpleXMLElement
    {
        $client = new Client();
        $response = $client->get("https://wwwinfo.mfcr.cz/cgi-bin/ares/darv_bas.cgi?ico={$ico}"); // @TODO adam

        return new SimpleXMLElement($response->getBody()->getContents());
    }
}
