<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\InvoiceRequest;
use App\Jobs\GenerateInvoice;
use App\Models\Invoice;
use App\Models\InvoiceList;
use App\Models\InvoiceSubscriber;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InvoiceController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Invoice
     */
    public function getInvoice(Request $request): Invoice
    {
        if($request->invoiceType !== Invoice::INVOICE_CREATE){
            return $this->getStoredInvoice($request);
        }

        return new Invoice();
    }

    /**
     * @param Request $request
     *
     * @return InvoiceSubscriber
     */
    public function getSubscriber(Request $request): InvoiceSubscriber
    {
        return InvoiceSubscriber::query()
            ->where('id', '=', $request->id)
            ->where('user_id', '=', Auth::user()->id)
            ->firstOrFail();
    }

    /**
     * @param InvoiceRequest $request
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function sendInvoice(InvoiceRequest $request): RedirectResponse
    {
        $invoiceSubscriber = InvoiceSubscriber::query()->create($this->mapInvoiceSubscriber($request));

        $invoice = $this->getStoreOrUpdateInvoice($request, $invoiceSubscriber);

        $invoice->invoiceLists()->delete();

        if(!$request->generateInvoice) {
            return redirect()->route('invoiceList');
        }

        foreach (json_decode($request->generateInvoice) as $item){
            InvoiceList::create([
                'invoice_id' => $invoice->id,
                'counter' => $item->counter,
                'pc' => $item->pc,
                'note' => $item->note,
                'price' => $item->price,
                'total_price' => $item->totalPrice,
            ]);
        }

        session()->flash('invoice-alert', [
            'type' => Invoice::SESSION_BY_EVENT[$request->invoice_type]['type'],
            'message' => Invoice::SESSION_BY_EVENT[$request->invoice_type]['message'],
        ]);

        dispatch(new GenerateInvoice($invoice));

        return redirect()->route('invoiceList');
    }

    /**
     * @param $request
     * @param $invoiceSubscriber
     *
     * @return Invoice
     * @throws \Exception
     */
    private function getStoreOrUpdateInvoice($request, $invoiceSubscriber): Invoice
    {
        if($request->invoice_type === Invoice::INVOICE_EDIT){
            $invoice = Invoice::query()->findOrFail($request->invoice_id);
            InvoiceSubscriber::query()->findOrFail($invoice->invoice_subscriber_id)->delete();
            $invoice->update($this->mapInvoiceData($request, $invoiceSubscriber));

            return $invoice;
        }

        return Invoice::query()->create($this->mapInvoiceData($request, $invoiceSubscriber));
    }

    /**
     * @param $request
     *
     * @return Invoice
     */
    private function getStoredInvoice($request): Invoice
    {
        $invoice = Invoice::query()
            ->where('id', '=', $request->invoiceId)
            ->where('user_id', '=', Auth::user()->id)
            ->with('invoiceSubscriber')
            ->with('invoiceLists')
            ->firstOrFail();

        if($request->invoiceType === Invoice::INVOICE_EDIT){
            return $invoice;
        }

        $invoice->reference_number += 1;
        $invoice->evidence_number += 1;
        $diffInDays = $invoice->date_of_issue->diffInDays($invoice->due_date);
        $invoice->date_of_issue = Carbon::now()->format('Y-m-d');
        $invoice->due_date = $invoice->due_date_id === Invoice::CUSTOM_DUE_DATE_ID
            ? $invoice->date_of_issue->addDays($diffInDays)
            : Invoice::getDueDateByDueDateType(Carbon::now()->format('Y-m-d'), (int) $invoice->due_date_id);

        return $invoice;
    }

    /**
     * @param Request $request
     * @param InvoiceSubscriber $invoiceSubscriber
     *
     * @return array
     */
    private function mapInvoiceData(Request $request, InvoiceSubscriber $invoiceSubscriber): array
    {
        return [
            'user_id' => Auth::user()->id,
            'type_id' => $request->invoiceType,
            'design_type_id' => 1,
            'rounding_type_id' => 1,
            'invoice_currency_id' => $request->currency,
            'invoice_language_id' => 1,
            'invoice_subscriber_id' => $invoiceSubscriber->id,
            'reference_number' => $request->invoiceNumber,
            'evidence_number' => $request->invoiceEvidentNumber,
            'date_of_issue' => $request->invoiceCreatedDate,
            'due_date_id' => $request->invoiceDueDateType,
            'due_date' => (int) $request->invoiceDueDateType === Invoice::CUSTOM_DUE_DATE_ID
                ? $request->invoiceDueDate
                : Invoice::getDueDateByDueDateType($request->invoiceCreatedDate, (int) $request->invoiceDueDateType),
            'type_of_payment' => $request->formOfPayment,
            'variable_symbol' => $request->variableSymbol,
            'founder' => $request->madeBy,
            'name' => $request->name,
            'ico' => $request->cin,
            'dic' => $request->tin,
            'street' => $request->street,
            'city' => $request->city,
            'postcode' => $request->zip,
            'bank_account_number' => $request->accountNumber,
            'iban_account_number' => $request->accountIban,
            'note' => $request->note,
            'index_note' => $request->registerInformation,
        ];
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    private function mapInvoiceSubscriber(Request $request): array
    {
        return [
            'user_id' => Auth::user()->id,
            'name' => $request->companyName,
            'ico' => $request->companyCin,
            'dic' => $request->companyTin,
            'street' => $request->companyStreet,
            'city' => $request->companyCity,
            'postcode' => $request->companyZip,
            'country_id' => $request->companyState,
        ];
    }
}
