<?php

declare(strict_types=1);

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\InvoiceCurrency;
use App\Models\InvoiceSubscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BasePageController extends Controller
{
    /**
     * @return array
     */
    protected function dataForInvoice(): array
    {
        $currencies = InvoiceCurrency::query()->pluck('name', 'id');
        $countries = Country::query()->pluck('name', 'id');
        $subscribers = InvoiceSubscriber::query()->where('user_id', '=', Auth::user()->id)->get();

        return [
            $currencies,
            $countries,
            $subscribers,
        ];
    }
}
