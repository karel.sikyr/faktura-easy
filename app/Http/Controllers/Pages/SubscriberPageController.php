<?php

declare(strict_types=1);

namespace App\Http\Controllers\Pages;

use Illuminate\Contracts\View\View;

class SubscriberPageController extends BasePageController
{
    /**
     * @return View
     */
    public function index(): View
    {
        [$currencies, $countries, $subscribers] = $this->dataForInvoice();
        $invoiceData = null;

        return view('subscribers', compact(
            'invoiceData',
            'currencies',
            'countries',
            'subscribers'
        ));
    }
}
