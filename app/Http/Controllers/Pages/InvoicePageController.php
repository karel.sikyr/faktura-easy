<?php

declare(strict_types=1);

namespace App\Http\Controllers\Pages;

use App\Models\Country;
use App\Models\Invoice;
use App\Models\InvoiceCurrency;
use App\Models\InvoiceSubscriber;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Response;

class InvoicePageController extends BasePageController
{
    /**
     * @return View
     */
    public function index(): View
    {
        $invoices = Invoice::query()
            ->where('user_id', '=', Auth::user()->id)
            ->orderByDesc('reference_number')
            ->paginate(20);
        return view('list', compact('invoices'));
    }

    /**
     * @return View
     */
    public function createInvoice(): View
    {
        $invoiceData = $this->getInvoiceData(null, Invoice::INVOICE_CREATE);
        [$currencies, $countries, $subscribers] = $this->dataForInvoice();

        return view('invoice', compact(
            'invoiceData',
            'currencies',
            'countries',
            'subscribers'
        ));
    }

    /**
     * @param int $id
     *
     * @return View
     */
    public function editInvoice(int $id): View
    {
        $invoiceData = $this->getInvoiceData($id, Invoice::INVOICE_EDIT);
        [$currencies, $countries, $subscribers] = $this->dataForInvoice();

        return view('invoice', compact(
            'invoiceData',
            'currencies',
            'countries',
            'subscribers'
        ));
    }

    /**
     * @param int $id
     *
     * @return View
     */
    public function copyInvoice(int $id): View
    {
        $invoiceData = $this->getInvoiceData($id, Invoice::INVOICE_COPY);
        [$currencies, $countries, $subscribers] = $this->dataForInvoice();

        return view('invoice', compact(
            'invoiceData',
            'currencies',
            'countries',
            'subscribers'
        ));
    }

    /**
     * @param int $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroyInvoice(int $id): RedirectResponse
    {
        $invoice = $this->getInvoiceQuery($id)->firstOrFail();
        $invoice->delete();
        session()->flash('invoice-alert', [
            'type' => 'danger',
            'message' => 'Faktura byla odstraněna'
        ]);
        return back();
    }

    public function downloadInvoice(int $id)
    {
        $invoice = $this->getInvoiceQuery($id)->firstOrFail();
        return Response::download($invoice->getFullPdfPath() . $invoice->reference_number.'.pdf');
    }

    /**
     * @param int|null $id
     * @param string $type
     *
     * @return array
     */
    private function getInvoiceData(?int $id, string $type): array
    {
        return [
            'id' => $id,
            'type' => $type
        ];
    }

    /**
     * @param int $id
     *
     * @return Builder
     */
    private function getInvoiceQuery(int $id): Builder
    {
        return Invoice::query()->where('id', '=', $id)->where('user_id', '=', Auth::user()->id);
    }
}
