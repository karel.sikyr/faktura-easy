<?php

declare(strict_types=1);

namespace App\Http\Controllers\Pages;

use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingPageController extends BasePageController
{
    /**
     * @return View
     */
    public function index(): View
    {
        $user = Auth::user();
        [$currencies, $countries, $subscribers] = $this->dataForInvoice();
        $invoiceData = null;

        return view('setting', compact(
            'invoiceData',
            'currencies',
            'countries',
            'subscribers',
            'user'
        ));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function updateSetting(Request $request): RedirectResponse
    {
        $user = User::query()->findOrFail(Auth::user()->id);

        $user->update([
            'name' => $request->name,
            'ico' => $request->cin,
            'dic' => $request->tin,
            'street' => $request->street,
            'city' => $request->city,
            'postcode' => $request->zip,
        ]);

        return back();
    }
}
