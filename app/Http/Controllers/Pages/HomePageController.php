<?php

declare(strict_types=1);

namespace App\Http\Controllers\Pages;

use Illuminate\Contracts\View\View;

class HomePageController extends BasePageController
{
    /**
     * @return View
     */
    public function index(): View
    {
        return view('welcome');
    }

    /**
     * @return View
     */
    public function lostPassword(): View
    {
        return view('lost-password');
    }

    /**
     * @return View
     */
    public function contact(): View
    {
        return view('contact');
    }
}
