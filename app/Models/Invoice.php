<?php

declare(strict_types=1);

namespace App\Models;

use Barryvdh\DomPDF\Facade as PdfFacade;
use Barryvdh\DomPDF\PDF;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use HasFactory;
    use Sluggable;
    use SoftDeletes;

    private const PDF_PATH = '/invoice_pdf/';
    private const PDF_VIEW = 'pdf';

    public const INVOICE_CREATE = 'create';
    public const INVOICE_EDIT = 'edit';
    public const INVOICE_COPY = 'copy';

    public const MONTH_DUE_DATE_ID = 5;
    public const CUSTOM_DUE_DATE_ID = 6;

    public const INVOICE_TYPES = [
        1 => 'Faktura bez DPH (nejsem plátce DPH)',
//        2 => 'Zálohová faktura'
    ];

    public const PAYMENT_TYPES = [
        1 => 'Bankovní převod',
//        2 => 'Kartou',
//        3 => 'Hotovost',
//        4 => 'Dobírka',
    ];

    public const DUE_DATE_TYPES = [
        1 => 'Dnes',
        2 => 'Týden',
        3 => '10 dní',
        4 => '14 dní',
        self::MONTH_DUE_DATE_ID => 'Měsíc',
        self::CUSTOM_DUE_DATE_ID => 'Vlastní',
    ];

    public const ADD_DAYS_BY_DUE_DATE_TYPE = [
        1 => 0,
        2 => 7,
        3 => 10,
        4 => 14,
    ];

    public const DESIGN_TYPES = [
        1 => 'Jednoduchý',
        2 => 'Moderní'
    ];

    public const ROUNDING_TYPES = [
        1 => 'Žádné',
        2 => 'Nejbližších 10 haléřů',
        3 => 'Celá čísla'
    ];

    public const SESSION_BY_EVENT = [
        self::INVOICE_CREATE => [
            'type' => 'success',
            'message' => 'Faktura byla vytvořena'
        ],
        self::INVOICE_EDIT => [
            'type' => 'success',
            'message' => 'Faktura byla aktualizována'
        ],
        self::INVOICE_COPY => [
            'type' => 'success',
            'message' => 'Faktura byla vytvořena'
        ],
    ];

    protected $guarded = [];

    protected $casts = [
        'date_of_issue' => 'date',
        'due_date' => 'date'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * @return string
     */
    public function getInvoiceTypeAttribute(): string
    {
        return self::INVOICE_TYPES[$this->type_id];
    }

    /**
     * @return string
     */
    public function getInvoicePaymentTypeAttribute(): string
    {
        return self::PAYMENT_TYPES[$this->type_of_payment];
    }

    /**
     * @return string
     */
    public function getFullPdfPath(): string
    {
        return public_path() . self::PDF_PATH . $this->user_id . '/';
    }

    /**
     * @param string $entryDate
     * @param int $dueDateType
     *
     * @return Carbon
     */
    public static function getDueDateByDueDateType(string $entryDate, int $dueDateType): Carbon
    {
        if(self::MONTH_DUE_DATE_ID === $dueDateType){
            return Carbon::parse($entryDate)->addMonth();
        }

        return Carbon::parse($entryDate)->addDays(self::ADD_DAYS_BY_DUE_DATE_TYPE[$dueDateType]);
    }

    /**
     * @return BelongsTo
     */
    public function invoiceSubscriber(): BelongsTo
    {
        return $this->belongsTo(InvoiceSubscriber::class);
    }

    /**
     * @return HasMany
     */
    public function invoiceLists(): HasMany
    {
        return $this->hasMany(InvoiceList::class);
    }

    /**
     * @param string $view
     * @return PDF
     */
    public function preparePdf($view = self::PDF_VIEW): PDF
    {
        $invoice = $this;
        return PdfFacade::loadView($view, compact('invoice'));
    }
}
